{$ifdef fpc}
  {$mode objfpc}{$H+}
  {$inline on}

  {$define UsePChar}
  {$define HasInt64}
{$endif}

{$IF DEFINED(FPC_FULLVERSION) and (FPC_FULLVERSION>30100)}
  {$warn 6058 off} // cannot inline
{$ENDIF}

